<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
    xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xpath-default-namespace="http://maven.apache.org/POM/4.0.0" exclude-result-prefixes="xs xsi"
    version="2.0">
   
    <xsl:output method="xml" indent="yes"/>

    <xsl:param name="value"  required="yes"/>
    <xsl:param name="property"  required="yes"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates mode="#current"/>
        </xsl:copy>
    </xsl:template>

    <!-- no properties element yet -->
    <xsl:template match="project[not(properties)]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <properties>
                <xsl:call-template name="generate-property-element"/>
            </properties>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- properties element exists, $property is not defined yet -->
    <xsl:template match="properties[not(*[local-name() eq $property])]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:call-template name="generate-property-element"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- $property is defined. -->
    <xsl:template match="*[local-name() eq $property]">
        <xsl:copy>
            <xsl:value-of select="$value"/>
        </xsl:copy>
        <xsl:message>Warning: Changing <xsl:value-of select="$property"/> from <xsl:value-of
                select="."/> to <xsl:value-of select="$value"/>. </xsl:message>
    </xsl:template>

    <!-- replace value with $property -->
    <xsl:template match="text()[starts-with(normalize-space(.), normalize-space($value))]">
        <xsl:value-of select="replace(., $value, concat('\${', $property,'}'))"/>
    </xsl:template>

    <xsl:template name="generate-property-element">
        <xsl:element name="{$property}">
            <xsl:value-of select="$value"/>
        </xsl:element>
    </xsl:template>


</xsl:stylesheet>
