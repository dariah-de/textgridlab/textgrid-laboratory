#!/bin/bash
#
# find and copy new or changed translation files from collect dir to origin location
# use after collect_translation.sh

COLLECT_DIR=translations
SLASH_REPLACE=_._

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")
OLDDIR=`pwd`

if [ ! -d $DIR/$COLLECT_DIR ]; then
  echo "no dir $DIR/$COLLECT_DIR found"
  exit
fi

cd $DIR/$COLLECT_DIR

for propfile in *; do
  fileloc=$DIR/${propfile//_._/\/}
  #echo $fileloc
  if [ ! -e $fileloc ]; then
    echo -e "[new file] $propfile \ncopying to $fileloc\n---"
    cp $propfile $fileloc
  elif ! diff -q $propfile $fileloc > /dev/null; then
    echo -e "[changed file] $propfile \ncopying to $fileloc\n---"
    cp $propfile $fileloc
  fi
  
done

cd $OLDDIR
