#!/bin/bash
#
# collect all translation files in one dir
#
# preparation: (https://projects.gwdg.de/projects/textgrid-laboratory?jump=welcome)
#
#  git clone --recursive git://git.projects.gwdg.de/textgrid-laboratory.git
#  cd textgrid-laboratory
#  git checkout develop
#  git submodule update --remote

PROPFILE_MATCH="bundle*.properties plugin*.properties messages*.properties about*.properties"
COLLECT_DIR=translations
SLASH_REPLACE=_._

SCRIPT=$(readlink -f "$0")
DIR=$(dirname "$SCRIPT")
OLDDIR=`pwd`
cd $DIR

if [ ! -d $COLLECT_DIR ]; then
  echo "creating dir $COLLECT_DIR"
  mkdir $COLLECT_DIR
fi

for prm in $PROPFILE_MATCH; do
  echo "collecting with pattern ${prm}"
  propfiles=`find . -name $prm`

  for propfile in $propfiles; do

    if [[ $propfile == *$SLASH_REPLACE* ]]; then
      echo "origin $propfile contains replacement pattern in path, you should change the variable SLASH_REPLACE"
    else
      newname=${propfile:2}
      newname=${newname//\//$SLASH_REPLACE}
      cp $propfile $COLLECT_DIR/$newname &> /dev/null
    fi
  done

done

cd $OLDDIR
echo "all translation files collected in directory $DIR/$COLLECT_DIR"


