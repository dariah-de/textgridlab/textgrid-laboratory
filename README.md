# TextGridLab Source Code

TextGridLab's source code can be found in the subprojects of this project, which are also included as git submodules to this project. To get the complete source code of the current TextGridLab development version, try the following:

        git clone --recursive -b develop https://gitlab.gwdg.de/dariah-de/textgridlab/textgrid-laboratory.git

Note that the `master` branch contains the current _stable_ version, while active development mostly happens in the `develop`  branch (and the respective `develop` branches of the submodules). So, if you have performed the step above and would like to look at the current stable source:


        cd textgrid-laboratory
        git checkout master
        git submodule update


# Building TextGridLab from the command line

Each submodule is a maven project that builds its p2 repository in <module>-repository/target when running `mvn clean package` in the component directory, the _base_ module additionally builds the TextGridLab product for all supported architectures. Each component fetches its dependencies from p2 update sites that are configured in the pom and can be reconfigured using properties.

The shell script `build-locally.sh` in this project runs all submodule builds such that they use each other's output as dependencies.

# Developing TextGridLab modules

Please have a look at https://dev2.dariah.eu/wiki/display/TextGrid/TextGridLab+Development+Environment to get up and running.


# Releasing the TextGridLab

Please have a look at https://wiki.de.dariah.eu/display/TextGrid/TextGridLab+Release+HOWTO to release.
