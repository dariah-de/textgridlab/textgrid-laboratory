#!/bin/bash
# 
# This script can be used to build a bunch of projects locally. Normally, the
# base lab will be built. The script is just a wrapper that calls maven a
# bunch of times
#
#############################################################################
# CONFIGURATION
#
# The projects to build. Order is significant!
PROJECTS="${PROJECTS-textgridlab-dependencies bundled-jre core help xmleditor linkeditor base}"
#
# repository root
REPO="${REPO-repo}"
#
# maven executable
MVN="${MVN-mvn}"
#
# maven default args, augmented by command line and repo path
MVN_ARGS="${MVN_ARGS-clean package}"
#
#############################################################################

if [ -t 1 ]
then
    bold=`tput bold`
    boldoff=`tput sgr0`
else
    bold=
    boldoff=
fi

die() {
    msg=$1
    exitcode=${2-1}

    echo
    echo "${bold}$msg${boldoff}"
    exit $exitcode
}
# WARNING NOT SAFE
repo_url="file:/`pwd`/$REPO"
cmdline="$MVN -Dlab.repository.root=${repo_url} -Dlab.repository.dependencies=${repo_url}/textgridlab-dependencies $MVN_ARGS $@"

cat <<EOF
$bold
Build Projects:		$PROJECTS
Command Line:		$cmdline
$boldoff
EOF

# Prepare repo
oldpwd="`pwd`"
mkdir -p "$REPO"
cd "$REPO"
for project in $PROJECTS
do
    ln -s -f -v ../$project/${project}-repository/target/repository $project
done
### Fix the dependency repo which is unfortunately named p2repository :-(
rm textgridlab-dependencies
ln -s -f -v ../textgridlab-dependencies/p2repository/target/repository textgridlab-dependencies
rm bundled-jre
ln -s -f -v ../bundled-jre/updatesite/target/repository bundled-jre

cd "$oldpwd"

for project in $PROJECTS
do
    (
	echo "${bold}Building $project ...${boldoff}"
	cd $project
	$cmdline || die "$cmdline => $?" $?
	echo
    ) || die "Error building $project: exiting ..." $?
done
exit $?
