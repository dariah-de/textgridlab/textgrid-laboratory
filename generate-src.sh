#!/bin/sh

help() {
cat <<EOH
$0 [outputfile.tar.gz]

Generates a tar.gz archive of all TextGridLab sources and saves it to the given
output file. If called with no argument, write to an auto-generated filename.

EOH
}

if [ -n "$1" ]
then
    case "$1" in
	-h|--help)
	    help
	    exit 1 ;;
	-*)
	    echo "ERROR: Unknown option: $1"
	    help
	    exit 2 ;;
	*)
	    finalfile="$1"
	    break ;;
    esac
fi





tardir=`mktemp -d`
export tardir
version=`git describe --tags`
mainfilename="TextGridLab-${version}.src.tar"
finalfile=${finalfile-"${mainfilename}.gz"}
collect="${tardir}/${mainfilename}"
export collect

echo "Archiving all source code to ${finalfile}."

git archive -o "${collect}" HEAD
git submodule foreach --recursive '
    git archive -o "${tardir}/${name}.tar" --prefix="${path}/" HEAD;
    tar --concatenate -f "${collect}" "${tardir}/${name}.tar";
    rm "${tardir}/${name}.tar"
    '
echo "Compressing ..."
gzip -c -v -N --rsyncable "${collect}" > ${finalfile}
echo "Deleting temporary files ..."
rm -rf "${tardir}"
echo "Done. "
