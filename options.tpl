# Debugging options for the org.eclipse.ui.intro.universal.

# Master flag for all org.eclipse.ui.intro.universal plugin debug options.
org.eclipse.ui.intro.universal/debug = true

# Enable logging of information messages in the plugin. By default, info
# messages are not logged. Setting this option to true will enable logging
# trace information messages.
org.eclipse.ui.intro.universal/trace/logInfo = true

# Enable logging of performance messages in the plugin. By default, performance
# messages are not logged. Setting this option to true will enable logging
# trace information messages. (note: enabling info logging does not enable
# this flag.)
org.eclipse.ui.intro.universal/trace/logPerformance = false

# Performance flags used by the Performance framework to report failures 
# of specific thresholds.  

# Time to create and display the full Intro view.
# org.eclipse.ui.intro/perf/createView = 1000

# Time needed to switch between Intro standby states.
# org.eclipse.ui.intro/perf/setStandbyState = 300# Debugging options for the org.eclipse.ui.intro.universal.

# Master flag for all org.eclipse.ui.intro.universal plugin debug options.
org.eclipse.ui.intro.universal/debug = true

# Enable logging of information messages in the plugin. By default, info
# messages are not logged. Setting this option to true will enable logging
# trace information messages.
org.eclipse.ui.intro.universal/trace/logInfo = true

# Enable logging of performance messages in the plugin. By default, performance
# messages are not logged. Setting this option to true will enable logging
# trace information messages. (note: enabling info logging does not enable
# this flag.)
org.eclipse.ui.intro.universal/trace/logPerformance = false

# Performance flags used by the Performance framework to report failures 
# of specific thresholds.  

# Time to create and display the full Intro view.
# org.eclipse.ui.intro/perf/createView = 1000

# Time needed to switch between Intro standby states.
# org.eclipse.ui.intro/perf/setStandbyState = 300# Debugging options for the org.eclipse.ui.intro.universal.

# Master flag for all org.eclipse.ui.intro.universal plugin debug options.
org.eclipse.ui.intro.universal/debug = true

# Enable logging of information messages in the plugin. By default, info
# messages are not logged. Setting this option to true will enable logging
# trace information messages.
org.eclipse.ui.intro.universal/trace/logInfo = true

# Enable logging of performance messages in the plugin. By default, performance
# messages are not logged. Setting this option to true will enable logging
# trace information messages. (note: enabling info logging does not enable
# this flag.)
org.eclipse.ui.intro.universal/trace/logPerformance = false

# Performance flags used by the Performance framework to report failures 
# of specific thresholds.  

# Time to create and display the full Intro view.
# org.eclipse.ui.intro/perf/createView = 1000

# Time needed to switch between Intro standby states.
# org.eclipse.ui.intro/perf/setStandbyState = 300# Debugging options for the org.eclipse.ui.intro.universal.

# Master flag for all org.eclipse.ui.intro.universal plugin debug options.
org.eclipse.ui.intro.universal/debug = true

# Enable logging of information messages in the plugin. By default, info
# messages are not logged. Setting this option to true will enable logging
# trace information messages.
org.eclipse.ui.intro.universal/trace/logInfo = true

# Enable logging of performance messages in the plugin. By default, performance
# messages are not logged. Setting this option to true will enable logging
# trace information messages. (note: enabling info logging does not enable
# this flag.)
org.eclipse.ui.intro.universal/trace/logPerformance = false

# Performance flags used by the Performance framework to report failures 
# of specific thresholds.  

# Time to create and display the full Intro view.
# org.eclipse.ui.intro/perf/createView = 1000

# Time needed to switch between Intro standby states.
# org.eclipse.ui.intro/perf/setStandbyState = 300# Debugging options for the org.eclipse.ui.intro.universal.

# Master flag for all org.eclipse.ui.intro.universal plugin debug options.
org.eclipse.ui.intro.universal/debug = true

# Enable logging of information messages in the plugin. By default, info
# messages are not logged. Setting this option to true will enable logging
# trace information messages.
org.eclipse.ui.intro.universal/trace/logInfo = true

# Enable logging of performance messages in the plugin. By default, performance
# messages are not logged. Setting this option to true will enable logging
# trace information messages. (note: enabling info logging does not enable
# this flag.)
org.eclipse.ui.intro.universal/trace/logPerformance = false

# Performance flags used by the Performance framework to report failures 
# of specific thresholds.  

# Time to create and display the full Intro view.
# org.eclipse.ui.intro/perf/createView = 1000

# Time needed to switch between Intro standby states.
# org.eclipse.ui.intro/perf/setStandbyState = 300# General debugging flags.
info.textgrid.lab.core.efs.tgcrud/debug=false

# Trace CRUD web service calls.
info.textgrid.lab.core.efs.tgcrud/debug/servicecalls=false

# Trace local file materialization.
info.textgrid.lab.core.efs.tgcrud/debug/localfile=false

# Log READ access to the Eclipse log file, including stack trace.
info.textgrid.lab.core.efs.tgcrud/debug/reads=false

# Trace ALL the input stream operations (of the logging input stream).
info.textgrid.lab.core.efs.tgcrud/debug/streams=false
# Use new features of the UI (like the ResultPage) 
info.textgrid.lab.core.importexport/new-gui = true# General model debugging.
info.textgrid.lab.core.model/debug=false

# Write out incompleteness stuff
info.textgrid.lab.core.model/debug/incomplete=false

# Verbosely dump incomplete metadata
info.textgrid.lab.core.model/debug/incomplete/verbose=false
 info.textgrid.lab.search/debug=false
info.textgrid.lab.search/debug/request-timing=false
info.textgrid.lab.search/debug/lazy-query=false# Debug the schema URI resolver 
info.textgrid.lab.xmleditor.mpeditor/debug/resolve=false
info.textgrid.lab.xmleditor.mpeditor/debug/resource-change=falsenet.sf.vex.editor/debug=false
net.sf.vex.editor/debug/config=false
net.sf.vex.editor/debug/layout=falsenet.sf.vex.toolkit/debug=false
net.sf.vex.toolkit/debug/nodeadapter=false
net.sf.vex.toolkit/debug/syncevents=false
net.sf.vex.toolkit/debug/selection=false
net.sf.vex.toolkit/debug/annotations=false
# Try to validate the linked model on various occasions
net.sf.vex.toolkit/debug/validate-model=false