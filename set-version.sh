#!/bin/sh

if [ "x$1" = "x" ]
then
    echo "Usage: $0 new-version [build-name]"
    exit 1
fi
version="$1"

if [ "$2" = "" ]
then
    productname="TextGridLab"
else
    productname="TextGridLab $2"
fi


echo Checking current SCM state ...
( cd base && git diff )
echo "Do you wish to set the TextGridLab version number to $version "
echo "and its build name to '$2' (empty = release)?"
echo -n "You shouldn't if there is output above. [Enter \"yes\" to continue] >"
read cont
if [ "$cont" != "yes" ]
then
    exit 1
fi

xmlstarlet ed -P -L -N m=http://maven.apache.org/POM/4.0.0 -u /m:project/m:version -v "${version}-SNAPSHOT" base/info.textgrid.lab.feature.base/pom.xml
xmlstarlet ed -P -L                                        -u /feature/@version -v "${version}.qualifier" base/info.textgrid.lab.feature.base/feature.xml
xmlstarlet ed -P -L -N m=http://maven.apache.org/POM/4.0.0 -u //m:archiveFileName -v "TextGridLab-${version}" base/base-repository/pom.xml
xmlstarlet ed -P -L                                        -u /product/@version -v "${version}.qualifier" base/base-repository/info.textgrid.lab.core.application.base_product.product
xmlstarlet ed -P -L                                        -u /product/@name -v "$productname" base/base-repository/info.textgrid.lab.core.application.base_product.product
xmlstarlet ed -P -L                                        -u '//product[@application="info.textgrid.lab.core.application"]/@name' -v "$productname" base/info.textgrid.lab.core.application/plugin.xml
xmlstarlet ed -P -L                                        -u '//property[@name="appName"]/@value' -v "$productname" base/info.textgrid.lab.core.application/plugin.xml

( cd base && git diff )
