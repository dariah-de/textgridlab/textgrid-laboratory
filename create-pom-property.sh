#!/bin/bash

script="`dirname \"$0\"`/create-pom-property.xsl"

if [ $# -lt 3 ]
then
    cat <<EOHELP
Error: At least 3 arguments required ($@)

Usage: $0 property value pomfiles ...

Creates the property <property> with the value <value> in each of the given
pom files. Replaces all text nodes beginning with <value> with a reference to
the property. If the property already exists, it is changed (and a warning
written to stdout)

EOHELP
    exit 1
fi
property="$1"
shift
value="$1"
shift

echo "Replacing value $value with property $property: " >&2

for f in "$@"; do
    echo "  in $f"
    tmp="`mktemp -t XXXXXXXXXX.pom.xml`"
    saxon "-s:$f" "-o:$tmp" "-xsl:$script" "property=${property}" "value=${value}"
    mv -b "$tmp" "$f"
done
